# Docker Image: vowo/typo3-flow #
   
You can check if your 
   container is running by running ```docker ps``` and see what port
   80 got mapped to. Then you should be able to access your application
   in your browser by going to http://(DOCKER-IP-HERE):port
   
## Example Dockerfile for your application ##
```
FROM eghernandez/docker-php7
MAINTAINER Your Name <your_email@domain.com>

ENV REFRESHED_AT 2016-01-02
ADD . /code
# Copy an Apache vhost file into sites-enabled. This should map
# the document root to whatever is right for your app

RUN rm -rf /etc/apache2/sites-enabled/000-default.conf
COPY vhost-config.conf /etc/apache2/sites-enabled/
```

## Example vhost file ##
```
<VirtualHost *:80>

  DocumentRoot /code/src/Web/
  SetEnv FLOW_CONTEXT Development
  #RewriteEngine On
  DirectoryIndex index.php

  <Directory /code/src/Web/>
    Options FollowSymLinks
    AllowOverride FileInfo Options=MultiViews
    Allow from all 
    Require all granted
  </Directory>

  LogLevel info
  ErrorLog /var/log/apache2/myapp-error.log
  CustomLog /var/log/apache2/myapp-access.log combined

</VirtualHost>

<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>
```